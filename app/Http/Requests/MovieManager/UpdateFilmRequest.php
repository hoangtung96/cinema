<?php

namespace App\Http\Requests\MovieManager;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateFilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationDatas = $this->validationData();
        return [
            'name' => [
                'required',
                Rule::unique('films', 'name')->ignore($validationDatas['id'])
            ],
            'category_id' => 'required|max:5',
            'description' => 'required',
            'release_year' => 'required|max:5|min:4',
        ];
    }
}
