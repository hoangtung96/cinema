<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class Films extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insert([
            ['name' => 'film 1', 'categorY_id' => 1,'release_year' => '2019', 'description' => 'film tv'],
            ['name' => 'film 2', 'categorY_id' => 1,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 3', 'categorY_id' => 1,'release_year' => '2015', 'description' => 'film tv'],
            ['name' => 'film 4', 'categorY_id' => 1,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 5', 'categorY_id' => 2,'release_year' => '2020', 'description' => 'film tv'],
            ['name' => 'film 6', 'categorY_id' => 2,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 7', 'categorY_id' => 2,'release_year' => '2015', 'description' => 'film tv'],
            ['name' => 'film 8', 'categorY_id' => 2,'release_year' => '2017', 'description' => 'film tv'],
            ['name' => 'film 9', 'categorY_id' => 3,'release_year' => '2020', 'description' => 'film tv'],
            ['name' => 'film 10', 'categorY_id' => 3,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 11', 'categorY_id' => 3,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 12', 'categorY_id' => 4,'release_year' => '2020', 'description' => 'film tv'],
            ['name' => 'film 13', 'categorY_id' => 4,'release_year' => '2021', 'description' => 'film tv'],
            ['name' => 'film 14', 'categorY_id' => 4,'release_year' => '2020', 'description' => 'film tv'],
        ]);
    }
}
