<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class Times extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('times')->insert([
            ['name' => 'frames 1', 'time_slot' => '9-11' ,'price' => 50000],
            ['name' => 'frames 2', 'time_slot' => '11-13','price' => 50000],
            ['name' => 'frames 3', 'time_slot' => '13-15','price' => 50000],
            ['name' => 'frames 4', 'time_slot' => '15-17','price' => 50000],
            ['name' => 'frames 5', 'time_slot' => '17-19','price' => 50000],
            ['name' => 'frames 6', 'time_slot' => '19-21','price' => 50000],
            ['name' => 'frames 7', 'time_slot' => '21-23','price' => 50000],
        ]);
    }
}
