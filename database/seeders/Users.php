<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            ['name' => 'admin', 'password' => Hash::make('123456'), 'is_manage' => 1, 'email' => 'tungh1929@gmail.com', 'token_email' => '1111ddddqs', 'cinema_id' => 1],
        );
    }
}
