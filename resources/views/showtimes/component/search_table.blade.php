
@if ($searchs->isNotEmpty())
    @foreach ( $searchs as $key => $roomfilm )
    <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $roomfilm->room_name }}</td>
        <td>{{ $roomfilm->film_name }}</td>
        <td>{{ $roomfilm->time_slot}} </td>
        <td>{{ $roomfilm->buyPrice}} VND</td>
        <td>{{ $roomfilm->day }}</td>
        @php
            $today = date('Y-m-d');
        @endphp
        <td>
            @if ($today < $roomfilm->day)
                <a class="btn btn-default" href="{{ route('Showtimes.edit', ['id' => $roomfilm->id]) }}">edit</a>
                <a class="btn btn-danger action_delete" data-url="{{ route('Showtimes.delete', ['id' => $roomfilm->id]) }}" >delete</a>
            @endif
        </td>
    </tr>
    @endforeach
    <div class="col-md-12" style="margin-top:30px" id="index">
    {{$searchs->links("pagination::bootstrap-4")}}
    </div>
@else
    <tr>
        <td>No matching results</td>
    </tr>
@endif
