
  <table class="table table-bordered" id="table_show">
    <thead>
      <tr>
        @for ($i = 1; $i <= $seats[0][0]['width']; $i++)
          <th>{{ $i }}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @foreach ( $seats as $hightSeats)
        <tr>
          @foreach ( $hightSeats as $withSeats )
            @if (!empty($seatsSelected))
                <td class="show_seats" style="background-color:{{ in_array( $withSeats['name'], $seatsSelected ) ? 'red': 'rgb(0, 255, 157)'}}" value="{{ $withSeats['name'] }}">{{ $withSeats['name'] }}</td>
            @else
                <td class="show_seats"  style="background-color:rgb(0, 255, 157)" value="{{ $withSeats['name'] }}">{{ $withSeats['name'] }}</td>
            @endif
          @endforeach
        </tr>
      @endforeach
    </tbody>
  </table>

  