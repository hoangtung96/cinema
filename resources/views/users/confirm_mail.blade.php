
@extends('users.form')

@section('title')
    confirm mail
@endsection

@section('content')
    <div id="login">
        <h3 class="text-center text-white pt-5">confirm mail</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    @if (session()->has('verifyMail'))
                        <p style="color: #ADFF2 ; text-align: center">{{ session('verifyMail') }}</p>
                    @endif
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{{ route('verify.mail') }}" method="post">
                            @csrf
                            <h3 class="text-center text-info">confirm</h3>
                            <div class="form-group">
                                <label class="text-info">email:</label><br>
                                <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group button_confirm">
                                <button type="submit" class="btn btn-info btn-md">next</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
