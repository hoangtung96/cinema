@extends('layouts.admin')

@section('title')
    create showfilm
@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-8">
            <h2>create showfilm</h2>
            <form action="{{ route('Showtimes.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlSelect2">name film</label>
                    <select class="form-control film_id" id="exampleFormControlSelect2" name="film_id">
                        @foreach ( $filmAll as $film)
                            <option value="{{ $film->id }}">{{ $film->name }}</option>
                        @endforeach
                    </select>
                    @error('film_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group" >
                    <label >day</label>
                    <input type="date" class="form-control-file day" name="day"  min="{{ date('Y-m-d') }}">
                    @error('day')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group" style="display: none" id="room">
                    <label for="exampleFormControlSelect2">room</label>
                    <select class="form-control room_id" id="exampleFormControlSelect2" name="room_id" data-url="{{ route('Showtimes.searchtime') }}">
                        <option id="option_select" value=""></option>
                        @foreach ( $roomAll as $room)
                            <option value="{{ $room->id }}">{{ $room->name }}</option>
                        @endforeach
                    </select>
                    @error('room_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group" style="display: none" id="frames">
                    <label for="exampleFormControlSelect2">frames</label>
                    <select class="form-control" id="select_box" name="time_id">
                        
                    </select>
                    @error('time_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>   

                <div class="form-group" style="display: none" id="price_difference">
                    <label >price difference %</label>
                    <input type="number" class="form-control-file" name="price_difference" value=0>
                    @error('price_difference')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <div class="col-md-6 ">
                        <a type="submit" class="btn btn-primary back_showtime" href="{{ route('Showtimes.index') }}">Back ShowTime </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(document).ready(function () {

        $(function(){
            $('.film_id').select2({
                selectOnClose: true
            });
        })

        $('.day').change(function () {
            let day = $(this).val();
            if(day){
                $('#room').show();
                $('#frames').show();
                $('#price_difference').show();
                $("#option_select").attr("selected","selected");
                $('#select_box').empty()
            } else {
                $('#room').hide();
                $('#frames').hide();
                $('#price_difference').hide();
            }
        })

        $('.room_id').change(function () {
            let day = $('.day').val();
            let room_id = $(this).val();
            let url = $(this).data('url');
            if (day && room_id && url) {
                $.ajax({
                    type: 'get',
                    url: url,
                    data: {
                        'room_id': room_id,
                        'day' : day,
                    },
                    success: function (data) {
                        if(!data.code){
                            $('#select_box').empty()
                            $("#select_box").append(data)
                        } else {
                            alert('error server')
                        }
                    },
                    error: function () {
                        alert('error server')
                    }
                })
            } else {
                $('#select_box').empty()
            } 
        })
    })
  </script>
@endsection



