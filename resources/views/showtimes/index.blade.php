@extends('layouts.admin')

@section('title')
    show times
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">showtime</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active">Showtime</li>
                </ol>
            </div>
            </div>
        </div>
        </div>

        <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                  <form action="{{ route('Showtimes.index') }}">
                    <div class="row">
                        <div class="col-md-2">
                          <label >room</label>
                          <input type="text" class="form-control" name="room_name" {{ !empty($params['room_name']) ? "value=".$params['room_name'] : "" }}>
                        </div>
                        <div class="col-md-2">
                            <label>name film</label>
                            <input type="text" class="form-control" name="film_name" {{ !empty($params['film_name']) ? " value=".$params['film_name'] : "" }} >
                        </div> 
                        <div class="col-md-2">
                          <label>start day</label>
                          <input type="date" class="form-control" name="start_day" {{ !empty($params['start_day']) ? "value=".$params['start_day'] : "" }}>
                        </div>
                        <div class="col-md-2">
                          <label>end day</label>
                          <input type="date" class="form-control" name="end_day" {{ !empty($params['end_day']) ? "value=".$params['end_day'] : "" }}>
                        </div>
                    </div>
                    <div class="row Search_add">
                      <div class="col-md1">
                        <button id="find" >Search</button>
                      </div>
                      <div class="col-md-11" >
                        <a class="btn btn-success" style="float: right" href="{{ route('Showtimes.create') }}">ADD</a>
                      </div class="col-md-6">
                    </div>
                  </form>
                  @if (session()->has('status'))
                    <p style="color: #008000">
                      {{ session('status') }}
                    </p> 
                  @endif 
                  @if (session()->has('error'))
                  <p style="color: red">
                    {{ session('status') }}
                  </p> 
                @endif 
                </div>
                
                <div class="col-md-12" id="tablefilm">
                    <table class="table">
                        <thead>
                          <th scope="col"></th>
                          <th scope="col">@sortablelink('room_id', 'Name Room')</th>
                          <th scope="col">@sortablelink('film_id', 'Name Film')</th>
                          <th scope="col">@sortablelink('time_id', 'Time Slot')</th>
                          <th scope="col">@sortablelink('price_difference', 'Price Buy')</th>
                          <th scope="col">@sortablelink('day', 'Day')</th>
                          <th></th>
                        </thead>
                        <tbody>
                          @if (!empty($searchs))
                            @include('showtimes.component.search_table')
                          @else
                            @include('showtimes.component.index_table')
                          @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('js')
  <script>
    $(document).ready(function () {


      $('.action_delete').click(function () {
        let url = $(this).data('url');
        let that = $(this);
        console.log(url);
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: 'get',
                    url: url,
                    success: function (data) {
                        if (data.code == 200) {
                            that.parent().parent().remove();
                            Swal.fire(
                                'Cancelled',
                                'Your showtimes has been deleted :)',
                                'success'
                            )
                        }
                    },
                    error: function () {  
                        Swal.fire(
                            'Cancelled',
                            'server error :)',
                            'error'
                        )
                    }
                })
            }
        })
      })
    })
  </script>
@endsection





