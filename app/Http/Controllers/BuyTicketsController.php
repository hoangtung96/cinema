<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Http\Requests\BuyTicket\TicketRequest;
use App\Models\RoomFilm;
use App\Models\Film;
use App\Models\Cutomer;
use App\Models\Bill;
use App\Models\Ticket;
use App\Models\Seat;
use App\Models\Cinema;
use Session;
use Auth;
use DB;


class BuyTicketsController extends Controller
{
    public $roomFilm;
    public $film;
    public $cutomer;
    public $bill;
    public $cinema;
    public function __construct(RoomFilm $roomFilm, Film $film, Cutomer $cutomer, Bill $bill, Ticket $ticket, Seat $seat, Cinema $cinema )
    {
        $this->roomFilm = $roomFilm;
        $this->film = $film;
        $this->cutomer = $cutomer;
        $this->bill = $bill;
        $this->ticket = $ticket;
        $this->seat = $seat;
        $this->cinema = $cinema;
    }

    public function index()
    {
        $today =  date('Y-m-d');
        $showFilms = $this->roomFilm->with(['film:id,name', 'time:id,time_slot'])
                                    ->where('day', $today)
                                    ->select('film_id')
                                    ->groupby('film_id')
                                    ->get();
        return view('buyTickets.index', compact('showFilms'));
    }

    public function searchTime(Request $request)
    {
        try {
            $filmId = $request->filmId;
            $today =  date('Y-m-d');
            $selectTimes = $this->roomFilm->leftJoin('films', 'films.id', '=', 'room_films.film_id')
                                        ->leftJoin('times', 'times.id', '=', 'room_films.time_id')
                                        ->where('room_films.day', $today)
                                        ->where('room_films.film_id', $filmId)
                                        ->select('room_films.id', 'times.time_slot', 'times.price', 'room_films.price_difference')
                                        ->get();
            return view('buyTickets.component.option_time_slot', compact('selectTimes'));
        } catch (Exception $e) {
            Log::error('message:' .$e->getMessage() . $e->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail'
            ], 500);
        }
    }

    public function searchRoom(Request $request)
    {
        try {
            $roomFilmId = $request->room_film_id;
            $selectTime = $this->roomFilm->leftJoin('rooms', 'room_films.room_id', '=', 'rooms.id')
                                        ->leftJoin('seats', 'rooms.id', '=', 'seats.room_id')
                                        ->where('room_films.id', $roomFilmId)
                                        ->select('rooms.width', 'seats.name')
                                        ->get()->toArray();

            // convert a large array to a small array
            $seats = array_chunk($selectTime, $selectTime[0]['width']);
            $cinemaId = Auth::user()->cinema_id;

            // get seats in session
            $key = "$cinemaId/"."$roomFilmId";
            $seatsSelected = session::get('slotSeats.'.$key);

            return view('buyTickets.component.show_seats', compact('seatsSelected', 'seats'));
        } catch (Exception $e) {
            Log::error('message:' .$e->getMessage() . $e->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail'
            ], 500);
        }
    }

    public function selectSeats(TicketRequest $request)
    { 
        // set key session
        $cinemaId = Auth::user()->cinema_id;
        $roomFilmId = (int) $request->room_film_id;
        $key = "$cinemaId/"."$roomFilmId";

        // convert Seats is string to array
        $seatsString = $request->seats[0];
        $SeatsArrays = explode(',', $seatsString);
        unset($SeatsArrays[0]);

        // get seats in session 
        $SeatsSold = !empty( Session()->get("slotSeats.".$key) ) ? Session()->get("slotSeats.".$key) : [];
        

        // check slot ticket
        $selectRoomFilm = $this->roomFilm
                                ->with(['time:id,price', 'room:id,width,height'])
                                ->where('id', $roomFilmId)->first();
        $maxSeat = ($selectRoomFilm->room->width)*($selectRoomFilm->room->height);

        if($maxSeat > count($SeatsSold)){
            if (empty( array_intersect($SeatsArrays, $SeatsSold) )) 
            {
                foreach ($SeatsArrays as $Seat)
                {
                    $request->Session()->push("slotSeats.".$key, $Seat);
                }
                try{
                    DB::beginTransaction();
                    $addCutomer = $this->cutomer->create([
                        'name' => $request->name,
                        'email'=> $request->email,
                        'recent_days'=> date('Y-m-d'),
                    ]);
                    
                    $seatIds = $this->seat->where('room_id', $selectRoomFilm->room_id)
                                    ->whereIn('name', $SeatsArrays)->get()
                                    ->pluck('id');
                    $priceTicket = ($selectRoomFilm->time->price + ($selectRoomFilm->time->price)*($selectRoomFilm->price_difference)*0.01);
                    $totalPriceBill = $priceTicket*($seatIds->count());

                    $addBill = $this->bill->create([
                        'cutomer_id' => $addCutomer->id,
                        'total_price'=> $totalPriceBill,
                    ]);

                    foreach ( $seatIds as $seatId)
                    {
                        $addTicket = $this->ticket->create([
                            'room_film_id' => $roomFilmId,
                            'seat_id' => $seatId,
                            'bill_id' => $addBill->id
                        ]);
                    }
                    DB::commit();
                    $infor = $this->roomFilm->leftJoin('rooms', 'room_films.room_id', '=', 'rooms.id')
                                            ->leftJoin('times', 'room_films.time_id', '=', 'times.id')
                                            ->leftJoin('films', 'room_films.film_id', '=', 'films.id')
                                            ->leftJoin('cinemas', 'rooms.cinema_id', '=', 'cinemas.id')
                                            ->where('room_films.id', $roomFilmId)
                                            ->select('cinemas.name as cinema_name', 'rooms.name as room_name',
                                                    'room_films.day', 'times.time_slot', 'films.name as film_name')
                                            ->first();
                    return view('buyTickets.ticket', compact('infor', 'priceTicket', 'SeatsArrays', 'totalPriceBill'));
                } catch (Exception $e){
                    DB::rollBack();
                    Log::error('message:' .$e->getMessage() . $e->getLine());
                }
            }
            session()->flash('error', 'seats sold');

            return redirect()->route('buy_tickets.index');
        }
        session()->flash('error', 'successful movie creation');

        return redirect()->route('sold out tickets');
        
    }
}
