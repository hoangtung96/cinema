<?php

namespace App\Http\Requests\Showtimes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'film_id' => 'required|numeric|exists:films,id',
            'room_id' => 'required|numeric|exists:rooms,id',
            'time_id' => 'required|numeric|exists:times,id',
            'day' => "required|Date|after_or_equal:today",
            'price_difference' => 'nullable|numeric'
        ];
    }
}
