
@extends('users.form')

@section('title')
    login
@endsection

@section('content')
    <div id="login">
        <h3 class="text-center text-white pt-5">Login cinema</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    @if (session()->has('login'))
                        <p style="color: red; text-align: center">{{ session('login') }}</p>
                    @endif
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{{ route('login.post') }}" method="post">
                            @csrf
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" class="form-control" value="{{ old('password') }}">
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group Forgot_password">
                                <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br>
                                <a href="{{ route('Forgot.password') }}" class="text-info">Forgot password</a>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Remember_me" class="btn btn-info btn-md" value="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
