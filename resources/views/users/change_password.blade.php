
@extends('users.form')

@section('title')
    change password
@endsection

@section('content')
    <div id="login">
        <h3 class="text-center text-white pt-5">change password</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        @php
                            $email = $_GET['email'];
                            $token_email = $_GET['token_email'];
                        @endphp
                        <form id="login-form" class="form" action="{{ route('update.password') }}" method="post">
                            @csrf
                            <input type="hidden" name="email" value="{{ $email }}">
                            <input type="hidden" name="token_email" value="{{ $token_email }}">
                            <h3 class="text-center text-info">change password</h3>
                            <div class="form-group">
                                <label class="text-info">new password:</label><br>
                                <input type="password" name="password" class="form-control" value="{{ old('password') }}">
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="text-info">verify password:</label><br>
                                <input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}">
                            </div>
                            <div class="form-group button_confirm">
                                <button type="submit" class="btn btn-info btn-md">summit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
