@extends('layouts.admin')

@section('title')
    movie manager
@endsection


@section('content')
    <div class="content-wrapper">
        <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">movie manager</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active">Movie Manager</li>
                </ol>
            </div>
            </div>
        </div>
        </div>

        <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                  <form action="{{ route('movie_manager.index') }}">
                    <div class="row">
                        <div class="col-md-3">
                            <label >film</label>
                            <input type="text" class="form-control" name="film_name" {{ !empty($params['film_name']) ? " value=".$params['film_name'] : "" }} >
                        </div> 
                        <div class="col-md-3">
                          <label >category</label>
                          <input type="text" class="form-control" name="category_name" {{ !empty($params['category_name']) ? "value=".$params['category_name'] : "" }}>
                        </div>
                    </div>
                    <div class="row Search_add">
                      <div class="col-md1">
                        <button id="find" >Search</button>
                      </div>
                      <div class="col-md-11" >
                        <a class="btn btn-success" style="float: right" href="{{ route('movie_manager.create') }}">ADD</a>
                      </div class="col-md-6">
                    </div>
                  </form>

                    <div>
                      @if (session()->has('status'))
                        <p style="color: #008000">
                          {{ session('status') }}
                        </p> 
                      @endif
                    </div>
                </div>

                <div class="col-md-12">
                  @if (!empty($searchFilms))
                    @include('MovieManager.searchfilms')
                  @else
                    @include('MovieManager.index_table')
                  @endif
                </div>

            </div>
        </div>
        </div>
    </div>
@endsection


@section('js')
  <script>
    $(document).ready(function () {


      $('.action_delete').click(function () {
        let url = $(this).data('url');
        let that = $(this);
        console.log(url);
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: 'get',
                    url: url,
                    success: function (data) {
                        if (data.code == 200) {
                            that.parent().parent().remove();
                            Swal.fire(
                                'Cancelled',
                                'Your movie has been deleted :)',
                                'success'
                            )
                        }
                    },
                    error: function () {
                        Swal.fire(
                            'Cancelled',
                            'server error :)',
                            'error'
                        )
                    }
                })
            }
        })
      })
    })
  </script>
@endsection


