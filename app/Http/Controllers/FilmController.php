<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Category;
use App\Http\Requests\MovieManager\CreateFilmRequest;
use App\Http\Requests\MovieManager\UpdateFilmRequest;

class FilmController extends Controller
{
    public $film;
    public $category;

    public function __construct(Film $film, Category $category)
    { 
        $this->film = $film;
        $this->category = $category;
    }

    /**
    * function select all films.
    *  
    *  @return response view
    */
    public function index(Request $request)
    { 
        if( empty($request->all()) ) {
            $moviePagination = $this->film->with('category:id,name')
                                        ->sortable()
                                        ->orderBy('films.name', 'ASC')
                                        ->orderBy('films.category_id', 'ASC')
                                        ->orderBy('films.release_year', 'ASC')
                                        ->paginate( $this->film::PAGINATE);

            return view('MovieManager.index', compact('moviePagination'));
        }

        $params = ['film_name' => $request->film_name, 'category_name' => $request->category_name];
        $data = $this->film->leftJoin('categories', 'films.category_id', '=', 'categories.id');
        if (!empty($params['film_name'])) {
            $data->Where('films.name', 'like', '%' . $params['film_name'] . '%');
        }
        if (!empty($params['category_name'])) {
            $data->Where('categories.name', 'like', '%' . $params['category_name'] . '%');
        }
        $searchFilms = $data->select('films.*', 'categories.name as category_name')
                            ->sortable()
                            ->orderBy('films.name', 'ASC')
                            ->orderBy('films.category_id', 'ASC')
                            ->orderBy('films.release_year', 'ASC')                
                            ->paginate( $this->film::PAGINATE);

        return view('MovieManager.index', compact('searchFilms', 'params'));
        
    }

    /**
    * function create film.
    *  
    *  @return response view
    */
    public function create()
    { 
        $categories = $this->category->all();
        return view('MovieManager.create', compact('categories'));
    }

    /**
    * function store film.
    *  @param array CreateFilmRequest
    *  @return route
    */
    public function store(CreateFilmRequest $request)
    { 
        $this->film->create([
            'name' => $request->name,
            'category_id' => (int) $request->category_id,
            'description' => $request->description,
            'release_year' => $request->release_year,
        ]);
        session()->flash('status', 'successful movie creation');

        return redirect()->route('movie_manager.index');
    }

    /**
    * function edit film.
    *  @param $id
    *  @return response view
    */
    public function edit($id)
    { 
        $film = $this->film->findOrFail($id);
        $categories = $this->category->all();

        return view('MovieManager.edit', compact('film', 'categories'));
    }


    /**
    * function update film.
    *  @param array UpdateFilmRequest, $id
    *  @return route
    */
    public function update($id, UpdateFilmRequest $request)
    { 
        $film = $this->film->findOrFail($id);
        $film->update([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'release_year' => $request->release_year,
        ]);
        session()->flash('status', 'successful movie edit');

        return redirect()->route('movie_manager.index');
    }

    /**
    * function delete film.
    *  @param $id
    *  @return response json
    */
    public function delete($id)
    { 
        try {
            $this->film->findOrFail($id)->delete();
            return response()->json([
             'code' => 200,
             'message' => 'success'
             ], 200);
 
        } catch (Exception $e) {
            Log::error('message:' .$e->getMessage() . $e->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail'
            ], 500);
        }
    }
}
