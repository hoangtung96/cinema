<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            Cinemas::class,
            Rooms::class,
            Seats::class,
            Times::class,
            Users::class,
            Films::class,
            Categories::class,
        ]);
    }
}
