<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResertPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6|max:8|confirmed',
            'token_email' => 'required|min:9|max:11',
            'email' => 'required|email',
        ];
    }
}
