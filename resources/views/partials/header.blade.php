<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <div class="col-md-12">
    <form action="{{ route('users.logout') }}" method="post">
      @csrf
      <button class="btn" style="float: right">logout</button>
    </form>
  </div>
</nav>