<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('test', function () {
//     return view('buyTickets.test');
// })->name('home');

Route::get('login', 'UserController@loginForm')->name('login.form');
Route::post('loginpost', 'UserController@loginPost')->name('login.post');

Route::middleware(['middleware' => 'Guest'])->group(function () {
    Route::get('Forgot_password', 'UserController@forgotPassword')->name('Forgot.password');
    Route::post('verify_mail', 'UserController@verifyMail')->name('verify.mail');
    Route::get('reset_password', 'UserController@resetPassword')->name('reset.password');
    Route::post('update_password', 'UserController@updatePassword')->name('update.password');
});

Route::middleware(['middleware' => 'CheckLogin'])->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('home', 'UserController@index')->name('home');
        Route::post('logout', 'UserController@logOut')->name('users.logout');

        Route::prefix('movie_manager')->group(function () {
            Route::get('index', 'FilmController@index')->name('movie_manager.index');
            Route::get('create', 'FilmController@create')->name('movie_manager.create');
            Route::post('store', 'FilmController@store')->name('movie_manager.store');
            Route::get('edit/{id}', 'FilmController@edit')->name('movie_manager.edit');
            Route::post('update/{id}', 'FilmController@update')->name('movie_manager.update');
            Route::get('delete/{id}', 'FilmController@delete')->name('movie_manager.delete');
            Route::get('search', 'FilmController@search')->name('movie_manager.search');
        });

        Route::prefix('Showtimes')->group(function () {
            Route::get('index', 'ShowtimesCotroller@index')->name('Showtimes.index');
            Route::get('create', 'ShowtimesCotroller@create')->name('Showtimes.create');
            Route::post('store', 'ShowtimesCotroller@store')->name('Showtimes.store');
            Route::get('edit/{id}', 'ShowtimesCotroller@edit')->name('Showtimes.edit');
            Route::post('update/{id}', 'ShowtimesCotroller@update')->name('Showtimes.update');
            Route::get('delete/{id}', 'ShowtimesCotroller@delete')->name('Showtimes.delete');
            Route::get('search_time', 'ShowtimesCotroller@searchTime')->name('Showtimes.searchtime');
        });

        Route::prefix('buy_tickets')->group(function () {
            Route::get('index', 'BuyTicketsController@index')->name('buy_tickets.index');
            Route::get('selectseats', 'BuyTicketsController@selectSeats')->name('Showtimes.selectseats');
            Route::get('search_time', 'BuyTicketsController@searchTime')->name('buy_tickets.searchtime');
            Route::get('search_room', 'BuyTicketsController@searchRoom')->name('buy_tickets.search_room');
        });


    });
});
