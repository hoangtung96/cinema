<?php

namespace App\Http\Requests\BuyTicket;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // exists:room_films,id
        return [
            'filmId' => 'required|exists:films,id',
            'room_film_id' => 'required|exists:room_films,id',
            'seats' => 'required',
            'name' => 'required',
            'email' => 'required|email',
        ];
    }
}
