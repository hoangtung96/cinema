$(document).ready(function () {

    $('#find').click(function (e) {
        let url = $(this).data('url');
        let params = $('.film_id').val();
        
        $.ajax({
            type: 'get',
            url: url,
            data: {
                'params': params
            },
            success: function (data) {
                if (data['code'] == 500)
                {
                    Swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                    return;
                }
                $('#tablefilm').children().remove();
                $('#tablefilm').append(data);
            },
            error: function () {
                Swal.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    })
})


 

