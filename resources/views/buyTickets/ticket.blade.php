@extends('layouts.admin')

@section('title')
    print tickets
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-10">
            <h2>print ticket</h2>
            <form action="" method="GET">
                @csrf
                    <div class="row table_ticket">
                        @foreach ( $SeatsArrays as $Seat )
                            <div class="col-md-5">
                                <h3>ticket</h3>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th scope="row">NAME CINEMA :</th>
                                        <td>{{ $infor->cinema_name }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">NAME ROOM :</th>
                                        <td>{{ $infor->room_name }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">DAY :</th>
                                        <td>{{ $infor->day }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">TIME SHOW :</th>
                                        <td>{{ $infor->time_slot }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">NAME FILM :</th>
                                        <td>{{ $infor->film_name }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">SEAT :</th>
                                        <td>{{ $Seat }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">PRICE :</th>
                                        <td>{{ $priceTicket }} VND</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>-------------------------------------------------------------------------------</p>
                            </div>
                        @endforeach
                    </div> 
                    <div class="row">
                        <div class="col-md-8 table_bill">
                            <h3>bill</h3>
                            <h4>CINEMA : {{ $infor->cinema_name }}</h4>
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">NAME FILM</th>
                                    <th scope="col">NAME ROOM</th>
                                    <th scope="col">TIME SHOW</th>
                                    <th scope="col">SEAT</th>
                                    <th scope="col">PRICE</th>
                                    <th scope="col">DAY</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($SeatsArrays as $key => $Seat)
                                        <tr>
                                            <td>{{ $infor->film_name }}</td>
                                            <td>{{ $infor->room_name }}</td>
                                            <td>{{ $infor->time_slot }}</td>
                                            <td>{{ $Seat }}</td>
                                            <td>{{ $priceTicket }}</td>
                                            <td>{{ $infor->day }}</td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td>TOTALPRICE : </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $totalPriceBill }} VND</td>
                                            <td></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <a class="btn btn-primary print" href="">print</a>
            </form>
        </div>
    </div>
@endsection

@section('js')
  <script>
       $(document).ready(function () {

        $(".print").on('click', function(e) {
            e.preventDefault()
            let contenTicket = $('.table_ticket').html();
            let contenbill = $('.table_bill').html();
            let newWin=window.open('','Print-Window');
            newWin.document.open();
            newWin.document.write(contenTicket);
            newWin.document.write(contenbill);
            newWin.document.close();

        });
    })
  </script>
@endsection