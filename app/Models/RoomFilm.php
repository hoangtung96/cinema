<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class RoomFilm extends Model
{
    use HasFactory;
    use Sortable;
    public $timestamps = false;
    const PAGINATE = 10;

    public $sortableAs = [
      'film_id',
      'room_id',
      'time_id',
      'day',
      'price_difference'
    ];

    public $fillable = [
      'film_id',
      'room_id',
      'time_id',
      'day',
      'price_difference'
    ];

    public function film()
    {
      return $this->belongsTo('App\Models\Film', 'film_id', 'id');
    }

    public function room()
    {
      return $this->belongsTo('App\Models\Room', 'room_id', 'id');
    }

    public function time()
    {
      return $this->belongsTo('App\Models\Time', 'time_id', 'id');
    }
}
