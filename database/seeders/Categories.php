<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;


class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'category 1', 'description' => 'categories'],
            ['name' => 'category 2', 'description' => 'categories'],
            ['name' => 'category 3', 'description' => 'categories'],
            ['name' => 'category 4', 'description' => 'categories'],
        ]);
    }
}
