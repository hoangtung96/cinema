<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class Cinemas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([
            ['name' => 'cinema 1', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 2', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 3', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 4', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 5', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 6', 'address' => 'xala', 'introduce' => '111111111'],
            ['name' => 'cinema 7', 'address' => 'xala', 'introduce' => '111111111'],
        ]);
    }
}
