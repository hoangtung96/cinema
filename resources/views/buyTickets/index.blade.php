@extends('layouts.admin')

@section('title')
    buy tickets
@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-8">
            <h2>ticket sales</h2>
            @if (session()->has('error'))
                <p style="color:red">
                {{ session('error') }}
                </p> 
            @endif
            <form action="{{ route('Showtimes.selectseats') }}" method="GET">
                @csrf

                {{-- select film --}}
                <div class="form-group">
                    <label for="exampleFormControlSelect2">name film</label>
                    <select class="form-control filmId" id="exampleFormControlSelect2" name="filmId" data-url={{ route('buy_tickets.searchtime') }}>
                        <option value=""></option>
                        @foreach ($showFilms as $roomflim)
                            <option value="{{ $roomflim->film_id }}">{{ $roomflim->film->name }}</option>
                        @endforeach
                    </select>
                    @error('filmId')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                {{-- select frames --}}
                <div class="form-group" id="frames">
                    <label for="exampleFormControlSelect2">frames</label>
                    <select class="form-control" id="room_film_id" name="room_film_id" data-url={{ route('buy_tickets.search_room') }}>

                    </select>
                    @error('room_film_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>   

                {{-- select seats --}}
                <div class="form-group" >
                    <label for="exampleFormControlSelect2">seats</label>
                    <input class="input_seats" type="hidden" name="seats[]">
                    <div id="append_table">

                    </div>
                </div> 

                {{-- infor cutomer name--}}
                <div class="form-group" >
                    <label for="exampleFormControlSelect2">name cutomer</label>
                    <input type="text" class="form-control-file" name="name">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div> 

                {{-- infor cutomer email --}}
                <div class="form-group" >
                    <label for="exampleFormControlSelect2">email</label>
                    <input type="text" class="form-control-file" name="email">
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div> 

                {{-- sum price --}}
                <div class="form-group" style="display: block" id="price">
                    <p>TOTAL PRICE : <p class="price">0</p> </p>
                    
                </div>  

                <button type="submit" class="btn btn-primary">buy tickets</button>
                <input type="hidden" id="selected_film_price">
            </form>
        </div>
    </div>
@endsection

@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(document).ready(function () {
        $(function(){
            $('.filmId').select2({
                selectOnClose: true
            });
        })

        $('.filmId').change(function () {
            let filmId = $(this).val();
            let url = $(this).data('url');
            if (filmId && url) {
                $.ajax({
                    type: 'get',
                    url: url,
                    data: {
                        'filmId': filmId,
                    },
                    success: function (data) {
                        if(!data.code){
                            $('#room_film_id').empty()
                            $('#append_table').empty()
                            $("#room_film_id").append(data)
                        } else {
                            alert('error server')
                        }
                    },
                    error: function () {
                        alert('error server')
                    }
                })
            } else {
                $('#room_film_id').empty()
            } 
        })

        $('#room_film_id').change(function () {
            let room_film_id = $(this).val();

            // frame selected
            let listFrames = JSON.parse($('#list_times').val())
            console.log('listFrames', listFrames)
            let timeSelected = listFrames.find(x => x.id == parseInt(room_film_id))
            if (timeSelected) {
                let priceResult = (timeSelected.price_difference / 100 * parseFloat(timeSelected.price)) + parseFloat(timeSelected.price)
                $('#selected_film_price').val(priceResult)
            }

            let url = $(this).data('url');
            if (room_film_id && url) {
                $.ajax({
                    type: 'get',
                    url: url,
                    data: {
                        'room_film_id': room_film_id,
                    },
                    success: function (data) {
                        if(!data.code){
                            $('#append_table').empty()
                            $('#append_table').append(data)
                        } else {
                            alert('error server')
                        }
                    },
                    error: function () {
                        alert('error server')
                    }
                })
            } else {
                $('#room_film_id').empty()
            } 
        })

        $(document).on('click', '.show_seats', function(e) {
            let seatsName = $(this).text();
            let old_data = $('.input_seats').val()
            var seatsArray = old_data.split(',');
            let color = $(this).css('backgroundColor');
            if ($(this).css('background-color') == "rgb(255, 0, 0)") 
                alert ('Seats have been sold');
            else {
                if ($.inArray(seatsName, seatsArray) != -1)
                {
                    // delete price
                    const index = seatsArray.indexOf(seatsName);
                    seatsArray.splice(index, 1);
                    let seatsString = seatsArray.toString();
                    $('.input_seats').val(seatsString)

                    let priceTickets = $('#selected_film_price').val();
                    let priceOld = $('.price').text();
                    let sumPrice = (parseFloat(priceOld) - parseFloat(priceTickets)) ;
                    $('.price').text(sumPrice)

                    $(this).css("background-color", "rgb(0, 255, 157)");
                } else {
                    // append price
                    $('.input_seats').val(old_data + ',' + seatsName)
                    let priceTickets = $('#selected_film_price').val();
                    let priceOld = $('.price').text();
                    let sumPrice = (parseFloat(priceOld) + parseFloat(priceTickets)) ;
                    $('.price').text(sumPrice)
                    $(this).css("background-color", "yellow");
                }
            }
        });
    })
  </script>
@endsection



