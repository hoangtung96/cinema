<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Showtimes\CreateShowRequest;
use App\Http\Requests\Showtimes\UpdateRequest;
use App\Models\Film;
use App\Models\Room;
use App\Models\Time;
use App\Models\RoomFilm;
use Auth;

class ShowtimesCotroller extends Controller
{
    public $film;
    public $room;
    public $time;
    public $roomfilm;
    public function __construct(Film $film, Room $room, Time $time, RoomFilm $roomfilm)
    {
        $this->film = $film;
        $this->room = $room;
        $this->time = $time;
        $this->roomfilm = $roomfilm;
    }

    public function select()
    {
        $filmAll = $this->film->all();
        $roomAll = $this->room->where('cinema_id', Auth::user()->cinema_id)->get();
        return [$filmAll, $roomAll];
    }

    public function index(Request $request)
    {    
        if( empty($request->room_name) && empty($request->film_name) && empty($request->start_day)  && empty($request->end_day)) {
            $roomfilms = $this->roomfilm->with(['film:id,name', 'room:id,name', 'time'])
                                        ->sortable()
                                        ->orderBy('room_films.room_id', 'ASC')
                                        ->orderBy('room_films.film_id', 'ASC')
                                        ->orderBy('room_films.time_id', 'ASC')
                                        ->orderBy('room_films.price_difference', 'ASC')
                                        ->orderBy('room_films.day', 'ASC')
                                        ->paginate( $this->roomfilm::PAGINATE);

            $roomfilms->getCollection()->transform(function($roomfilm){
                $roomfilm->buyPrice  = $roomfilm->time->price + ($roomfilm->price_difference)*0.01*($roomfilm->time->price);
                return $roomfilm;
            });   

            return view('showtimes.index', compact('roomfilms'));
        }

        $params = [
                    'room_name' => $request->room_name,
                    'film_name' => $request->film_name,
                    'start_day' => $request->start_day, 
                    'end_day' => $request->end_day
        ];
        $data = $this->roomfilm->rightjoin('films', 'films.id', '=', 'room_films.film_id')
                                ->rightjoin('rooms', 'rooms.id', '=', 'room_films.room_id')
                                ->rightjoin('times', 'times.id', '=', 'room_films.time_id');
        if (!empty($params['room_name'])) {
            $data->Where('rooms.name', 'like', '%' . $params['room_name'] . '%');
        }
        if (!empty($params['film_name'])) {
            $data->Where('films.name', 'like', '%' . $params['film_name'] . '%');
        }
        if (!empty($params['start_day']) && !empty($params['end_day'])) {
            $data->whereBetween('room_films.day', [ $params['start_day'], $params['end_day'] ]);
        } elseif (!empty($params['start_day']) && empty($params['end_day'])) {
            $data->where('room_films.day', '>=',$params['start_day'] );
        }elseif (empty($params['start_day']) && !empty($params['end_day'])) {
            $data->where('room_films.day', '<=',$params['end_day'] );
        }   
        $searchs = $data->select('films.name as film_name', 'rooms.name as room_name', 'times.time_slot', 'times.price', 'room_films.*')
                        ->sortable()
                        ->orderBy('room_films.room_id', 'ASC')
                        ->orderBy('room_films.film_id', 'ASC')
                        ->orderBy('room_films.time_id', 'ASC')
                        ->orderBy('room_films.price_difference', 'ASC')
                        ->orderBy('room_films.day', 'ASC')
                        ->paginate( $this->roomfilm::PAGINATE);

        $searchs->getCollection()->transform(function($roomfilm){
            $roomfilm->buyPrice  = $roomfilm->time->price + ($roomfilm->price_difference)*0.01*($roomfilm->time->price);
            return $roomfilm;
        });   

        return view('showtimes.index', compact('searchs', 'params'));
    }

    public function create()
    {
        list($filmAll, $roomAll) = $this->select();
        $timeAll = $this->time->all();

        return view('showtimes.create', compact('filmAll', 'roomAll', 'timeAll'));
    }

    public function store(CreateShowRequest $request)
    {
        $checkRoomFilm = $this->roomfilm->where('day', $request->time_id)
                                        ->where('room_id', $request->room_id)
                                        ->where('time_id', $request->time_id)
                                        ->first();
        if(empty($checkRoomFilm))
        {
            $add = $this->roomfilm->create([
                'film_id' => (int) $request->film_id,
                'room_id' => (int) $request->room_id,
                'time_id' => (int) $request->time_id,
                'day' => $request->day,
                'price_difference' => !empty($request->price_difference) ? $request->price_difference : 0 ,
            ]);
            session()->flash('status', 'successful showtime creation');
    
            return redirect()->route('Showtimes.index');
        }
        session()->flash('error', 'error showtime update');

        return redirect()->route('Showtimes.index');
    }

    public function searchTime(Request $request)
    {
        try {
            $time_ids = $this->roomfilm->where('day', $request->day)
                                        ->where('room_id', $request->room_id)
                                        ->select('time_id')
                                        ->get()->toArray();
            $times = $this->time->whereNotIn('id', $time_ids)->get();
            
            return view('showtimes.component.search_time', compact('times'));
        } catch (Exception $e) {
            Log::error('message:' .$e->getMessage() . $e->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail'
            ], 500);
        }
    }

    public function edit($id)
    {
        $today = date('Y-m-d');
        $room_film = $this->roomfilm->with(['film:id,name', 'room:id,name', 'time'])->findOrFail($id);
        if ( $room_film->day > $today)
        {
            list($filmAll, $roomAll) = $this->select();
            $timeIds = $this->roomfilm->where('day', $room_film->day)
                                        ->where('room_id', $room_film->room_id)
                                        ->where('time_id', '<>', $room_film->time_id)
                                        ->select('time_id')
                                        ->get()->toArray();
            $times = $this->time->whereNotIn('id', $timeIds)->get();
    
            return view('showtimes.edit', compact('room_film', 'filmAll', 'roomAll', 'times'));
        }

        return redirect()->route('Showtimes.index');
    }

    public function update($id, UpdateRequest $request)
    {
        $checkRoomFilm = $this->roomfilm->where('day', $request->time_id)
                                        ->where('room_id', $request->room_id)
                                        ->where('time_id', $request->time_id)
                                        ->where('id','<>', $id)
                                        ->first();
        if(empty($checkRoomFilm))
        {                              
            $this->roomfilm->findOrFail($id)->update([
                            'film_id' => $request->film_id,
                            'room_id' => $request->room_id,
                            'time_id' => $request->time_id,
                            'day' => $request->day,
                            'price_difference' => $request->price_difference,
            ]);
            session()->flash('status', 'successful showtime update');

            return redirect()->route('Showtimes.index');
        }

        session()->flash('error', 'error showtime update');

        return redirect()->route('Showtimes.index');
    }

    /**
    * function delete film.
    *  @param $id
    *  @return response json
    */
    public function delete($id)
    { 
        try {
            $this->roomfilm->findOrFail($id)->delete();
            return response()->json([
             'code' => 200,
             'message' => 'success'
             ], 200);
 
        } catch (Exception $e) {
            Log::error('message:' .$e->getMessage() . $e->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail'
            ], 500);
        }
    }

}
