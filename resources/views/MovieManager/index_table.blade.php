<table class="table">
    <thead>
      <tr>
        <th scope="col"></th>
        <th scope="col">@sortablelink('Name')</th>
        <th scope="col">@sortablelink('Category_id', 'categories Name')</th>
        <th scope="col">@sortablelink('description', 'Description')</th>
        <th scope="col">@sortablelink('Release_year', 'Release Year')</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        @foreach ( $moviePagination as $key => $film )
        <tr>
          <th scope="row">{{ $key + 1 }}</th>
          <td>{{ $film->name }}</td>
          <td>{{ $film->category->name }}</td>
          <td>{{ $film->description }}</td>
          <td>{{ $film->release_year }}</td>
          <td>
            <a class="btn btn-default" href="{{ route('movie_manager.edit', ['id' => $film->id]) }}">edit</a>
            <a class="btn btn-danger action_delete" data-url="{{ route('movie_manager.delete', ['id' => $film->id]) }}" >delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
    <div class="col-md-12" style="margin-top:30px" id="index">
        {{$moviePagination->links("pagination::bootstrap-4")}}
    </div>
</table>