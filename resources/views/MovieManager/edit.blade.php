@extends('layouts.admin')

@section('title')
    edit film
@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-8">
            <h2>edit film</h2>
            @php
                $id = (int) request()->id;
            @endphp
            <form action="{{ route('movie_manager.update',['id' => $film->id]) }}" method="POST">
                @csrf
                <div class="form-group">
                    <label >name</label>
                    <input type="text" class="form-control" name="name" value="{{ $film->name }}">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <input type="hidden" value="{{ $id }}" name="id">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">caetegory</label>
                    <select class="form-control" id="exampleFormControlSelect2" name="category_id">
                        @foreach ( $categories as $category)
                            <option {{ $film->category_id == $category->id ? 'selected' : ''  }} value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('caetegory_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">description</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3">{{ $film->description }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>                
                <div class="form-group">
                    <label >release year</label>
                    <input type="number" class="form-control-file" name="release_year" value="{{ $film->release_year }}">
                    @error('release_year')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection


