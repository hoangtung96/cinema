<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class Rooms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            ['name' => 'room 1', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 2', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 3', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 4', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 5', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 6', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],
            ['name' => 'room 7', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 1],

            ['name' => 'room 1', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 2', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 3', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 4', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 5', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 6', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            ['name' => 'room 7', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 2],
            
            ['name' => 'room 1', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 2', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 3', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 4', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 5', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 6', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
            ['name' => 'room 7', 'width' => 5,'height' => 6,'description' => 'room', 'cinema_id' => 3],
        ]);
    }
}
