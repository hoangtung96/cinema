<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Room extends Model
{
    use HasFactory;
    use Sortable;
    public $timestamps = false;
    public $sortable = [
        'name',
    ];
    public $fillable  = [
        'name',
        'max_seats',
        'cinema_id',
    ];
}
