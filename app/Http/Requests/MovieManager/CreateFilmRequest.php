<?php

namespace App\Http\Requests\MovieManager;

use Illuminate\Foundation\Http\FormRequest;

class CreateFilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:films',
            'category_id' => 'required|max:5',
            'description' => 'required',
            'release_year' => 'required|max:5|min:4',
        ];
    }
}
