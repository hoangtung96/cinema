<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ConfimMailRequest;
use App\Http\Requests\ResertPasswordRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendResetPassword;
use Mail;
use Hash;

class UserController extends Controller
{
    public $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return view('home');
    }

    /**
    * load form login.
    *
    * @return response view
    */
    public function loginForm()
    {
        // $hashed = Hash::make('123456');
        // dd($hashed);
        return view('users.login');
    }

    /**
    * summit form login.
    * @param array LoginUserRequest
    * @return route
    */
    public function loginPost(LoginUserRequest $request)
    {
        $remember = $request->has('remember_me') ? true : false;
        if(Auth()->attempt([
            'name' => $request->name,
            'password' => $request->password,
        ], $remember)){
            return redirect()->route('home');
        }
        session()->flash('login', 'Username or password incorrect');
        
        return redirect()->route('login.form');
    }

    /**
    * function logout.
    * @param array Request
    * @return route
    */
    public function logOut(Request $request)
    {
        Auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('login.form');
    }

    /**
    * function load form confirm mail.
    * 
    *  @return response view
    */
    public function forgotPassword()
    {
        return view('users.confirm_mail');
    }

    /**
    * function verify mail.
    *  @param array ConfimMailRequest
    *  @return route
    */
    public function verifyMail(ConfimMailRequest $request)
    {
        $token_email =  Str::random(10);
        $user = $this->user->where('email', $request->email)->first();
        if (!empty($user))
        {
            $user->update(['token_email' => $token_email]);
            $data = [
                'title' => 'password retrieval',
                'text' => 'click here',
                'url' => url("/reset_password?email=$request->email&token_email=$token_email"),
            ];
            $user->notify(new SendResetPassword($data));
            session()->flash('verifyMail', 'please check your email');

            return redirect()->route('Forgot.password');
        }
        session()->flash('verifyMail', 'your email does not exist');
        return redirect()->route('Forgot.password');
    }

    /**
    * function load form reset mail.
    *  
    *  @return response view
    */
    public function resetPassword()
    {
       return view('users.change_password');
    }

    /**
    * function update Password.
    *  @param array ResertPasswordRequest
    *  @return route
    */
    public function updatePassword(ResertPasswordRequest $request)
    {
        $user = $this->user->where('email', $request->email)
                            ->where('token_email', $request->token_email)->first();
        if(!empty($user))
        {
            $newPassword = Hash::make($request->password);
            $user = $user->update(['password' => $newPassword]);   

            return redirect()->route('login.form');
        }
        return redirect()->route('reset.password');
    }
}
