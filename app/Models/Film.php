<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Film extends Model
{
    use HasFactory;
    use Sortable;
    public $timestamps = false;
    const PAGINATE = 10;

    public $sortableAs  = [
      'Name',
      'Category_id',
      'Release_year',
      'description'
    ];

    protected $fillable = [
        'name',
        'category_id',
        'release_year',
        'description',
    ];

    public function category()
    {
      return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
}
