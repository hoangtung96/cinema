FROM php:7.3.28-fpm-alpine
RUN apk update && apk add libxml2-dev
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install ctype
RUN docker-php-ext-install fileinfo
RUN docker-php-ext-install json
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install tokenizer
RUN docker-php-ext-install xml
RUN apk update && apk add nodejs npm
RUN docker-php-ext-install exif
RUN docker-php-ext-install fileinfo
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN install-php-extensions gd
RUN install-php-extensions sodium
RUN install-php-extensions zip
# RUN install-php-extensions Spreadsheet



# OpenSSL \

# composer
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /composer
RUN curl -sS https://getcomposer.org/installer \
    | php -- --install-dir=/usr/bin --filename=composer

