
@foreach ( $roomfilms as $key => $roomfilm )
    <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $roomfilm->room->name }}</td>
        <td>{{ $roomfilm->film->name }}</td>
        <td>{{ $roomfilm->time->time_slot}} </td>
        <td>{{ $roomfilm->buyPrice }} VND</td>
        <td>{{ $roomfilm->day }}</td>
        @php
            $today = date('Y-m-d');
        @endphp
        <td>
            @if ($today < $roomfilm->day)
                <a class="btn btn-default" href="{{ route('Showtimes.edit', ['id' => $roomfilm->id]) }}">edit</a>
                <a class="btn btn-danger action_delete" data-url="{{ route('Showtimes.delete', ['id' => $roomfilm->id]) }}" >delete</a>
            @endif
        </td>
    </tr>
@endforeach
<div class="col-md-12" style="margin-top:30px" id="index">
    {{$roomfilms->links("pagination::bootstrap-4")}}
</div>
